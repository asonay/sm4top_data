
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_v2","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII","ttbar_PhPy8_AFII_HF_filt"},"(( (((mcChannelNumber == 410471) || (mcChannelNumber == 410470 && !truth_top_dilep_filter) || (mcChannelNumber == 410472 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 0) || (((mcChannelNumber == 411079) || (mcChannelNumber == 411073 && !truth_top_dilep_filter) || (mcChannelNumber == 411076 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 1) || (((mcChannelNumber == 411080) || (mcChannelNumber == 411074 && !truth_top_dilep_filter) || (mcChannelNumber == 411077 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 2) || (((mcChannelNumber == 411081) || (mcChannelNumber == 411075 && !truth_top_dilep_filter) || (mcChannelNumber == 411078 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 3) ))")'

#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII","ttbar_PhPy8_AFII_HF_filt"},"(( (((mcChannelNumber == 410471) || (mcChannelNumber == 410470 && !truth_top_dilep_filter) || (mcChannelNumber == 410472 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 0) || (((mcChannelNumber == 411079) || (mcChannelNumber == 411073 && !truth_top_dilep_filter) || (mcChannelNumber == 411076 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 1) || (((mcChannelNumber == 411080) || (mcChannelNumber == 411074 && !truth_top_dilep_filter) || (mcChannelNumber == 411077 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 2) || (((mcChannelNumber == 411081) || (mcChannelNumber == 411075 && !truth_top_dilep_filter) || (mcChannelNumber == 411078 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 3) )) && nBTags_MV2c10_70>=3&&nJets>=10")'

#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII","ttbar_PhPy8_AFII_HF_filt"},"(( (((mcChannelNumber == 410471) || (mcChannelNumber == 410470 && !truth_top_dilep_filter) || (mcChannelNumber == 410472 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 0) || (((mcChannelNumber == 411079) || (mcChannelNumber == 411073 && !truth_top_dilep_filter) || (mcChannelNumber == 411076 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 1) || (((mcChannelNumber == 411080) || (mcChannelNumber == 411074 && !truth_top_dilep_filter) || (mcChannelNumber == 411077 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 2) || (((mcChannelNumber == 411081) || (mcChannelNumber == 411075 && !truth_top_dilep_filter) || (mcChannelNumber == 411078 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 3) )) && nBTags_MV2c10_70>=3&&nJets==9")'


#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII","ttbar_PhPy8_AFII_HF_filt"},"(( (((mcChannelNumber == 410471) || (mcChannelNumber == 410470 && !truth_top_dilep_filter) || (mcChannelNumber == 410472 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 0) || (((mcChannelNumber == 411079) || (mcChannelNumber == 411073 && !truth_top_dilep_filter) || (mcChannelNumber == 411076 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 1) || (((mcChannelNumber == 411080) || (mcChannelNumber == 411074 && !truth_top_dilep_filter) || (mcChannelNumber == 411077 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 2) || (((mcChannelNumber == 411081) || (mcChannelNumber == 411075 && !truth_top_dilep_filter) || (mcChannelNumber == 411078 && truth_top_dilep_filter)) && TopHeavyFlavorFilterFlag == 3) )) && nBTags_MV2c10_70>=3&&nJets==8")'

root -l -b -q 'CreateNtuple.cpp("4top_lo_simon_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"4top_lo"},"nBTags_MV2c10_70>=3&&nJets>=10")'
root -l -b -q 'CreateNtuple.cpp("4top_lo_simon_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"4top_lo"},"nBTags_MV2c10_70>=3&&nJets==9")'
root -l -b -q 'CreateNtuple.cpp("4top_lo_simon_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"4top_lo"},"nBTags_MV2c10_70>=3&&nJets==8")'

##
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII_AllFilt"},"nBTags_MV2c10_70>=3&&nJets>=10")'
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII_AllFilt"},"nBTags_MV2c10_70>=3&&nJets==9")'
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttbar_PhPy8_AFII_AllFilt"},"nBTags_MV2c10_70>=3&&nJets==8")'
#
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttbar_PhPy8_AFII_AllFilt"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttbar_PhPy8_AFII_AllFilt"},"nBTags_MV2c10_70>=3&&nJets==7")'
#root -l -b -q 'CreateNtuple.cpp("ttbar_PhPy8_AFII_AllFilt_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttbar_PhPy8_AFII_AllFilt"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#root -l -b -q 'CreateNtuple.cpp("4top_lo_simon_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"4top_lo_simon"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#root -l -b -q 'CreateNtuple.cpp("4top_lo_simon_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"4top_lo_simon"},"nBTags_MV2c10_70>=3&&nJets==7")'
#root -l -b -q 'CreateNtuple.cpp("4top_lo_simon_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"4top_lo_simon"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#root -l -b -q 'CreateNtuple.cpp("4top_aMcAtNloPy8_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"4top_aMcAtNloPy8"},"nBTags_MV2c10_70>=3&&nJets>=10")'
#root -l -b -q 'CreateNtuple.cpp("4top_aMcAtNloPy8_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"4top_aMcAtNloPy8"},"nBTags_MV2c10_70>=3&&nJets==9")'
#root -l -b -q 'CreateNtuple.cpp("4top_aMcAtNloPy8_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"4top_aMcAtNloPy8"},"nBTags_MV2c10_70>=3&&nJets==8")'
#
#root -l -b -q 'CreateNtuple.cpp("4top_aMcAtNloPy8_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"4top_aMcAtNloPy8"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#root -l -b -q 'CreateNtuple.cpp("4top_aMcAtNloPy8_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"4top_aMcAtNloPy8"},"nBTags_MV2c10_70>=3&&nJets==7")'
#root -l -b -q 'CreateNtuple.cpp("4top_aMcAtNloPy8_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"4top_aMcAtNloPy8"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#root -l -b -q 'CreateNtuple.cpp("ttcomb_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttcomb"},"nBTags_MV2c10_70>=3&&nJets>=10")'
#
#root -l -b -q 'CreateNtuple.cpp("ttcomb_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttcomb"},"nBTags_MV2c10_70>=3&&nJets==9")'
#
#root -l -b -q 'CreateNtuple.cpp("ttcomb_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttcomb"},"nBTags_MV2c10_70>=3&&nJets==8")'
#
#
#root -l -b -q 'CreateNtuple.cpp("data_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"data"},"nBTags_MV2c10_70>=3&&nJets>=10")'
###
#root -l -b -q 'CreateNtuple.cpp("data_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"data"},"nBTags_MV2c10_70>=3&&nJets==9")'
###
#root -l -b -q 'CreateNtuple.cpp("data_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"data"},"nBTags_MV2c10_70>=3&&nJets==8")'
##
##
##
#root -l -b -q 'CreateNtuple.cpp("ttX_simon_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttX_simon"},"nBTags_MV2c10_70>=3&&nJets>=10")'
##
#root -l -b -q 'CreateNtuple.cpp("ttX_simon_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttX_simon"},"nBTags_MV2c10_70>=3&&nJets==9")'
##
#root -l -b -q 'CreateNtuple.cpp("ttX_simon_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"ttX_simon"},"nBTags_MV2c10_70>=3&&nJets==8")'
##
##
##
#root -l -b -q 'CreateNtuple.cpp("vjets_simon_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"vjets_simon"},"nBTags_MV2c10_70>=3&&nJets>=10")'
##
#root -l -b -q 'CreateNtuple.cpp("vjets_simon_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"vjets_simon"},"nBTags_MV2c10_70>=3&&nJets==9")'
##
#root -l -b -q 'CreateNtuple.cpp("vjets_simon_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"vjets_simon"},"nBTags_MV2c10_70>=3&&nJets==8")'
##
##
##
#root -l -b -q 'CreateNtuple.cpp("vv_simon_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"vv_simon"},"nBTags_MV2c10_70>=3&&nJets>=10")'
##
#root -l -b -q 'CreateNtuple.cpp("vv_simon_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"vv_simon"},"nBTags_MV2c10_70>=3&&nJets==9")'
##
#root -l -b -q 'CreateNtuple.cpp("vv_simon_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"vv_simon"},"nBTags_MV2c10_70>=3&&nJets==8")'
##
##
##
##
#root -l -b -q 'CreateNtuple.cpp("single-top_simon_10ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"single-top_simon"},"nBTags_MV2c10_70>=3&&nJets>=10")'
##
#root -l -b -q 'CreateNtuple.cpp("single-top_simon_9je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"single-top_simon"},"nBTags_MV2c10_70>=3&&nJets==9")'
#
#root -l -b -q 'CreateNtuple.cpp("single-top_simon_8je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",{"single-top_simon"},"nBTags_MV2c10_70>=3&&nJets==8")'
##
##
##
##
##
##
##
##
#root -l -b -q 'CreateNtuple.cpp("ttcomb_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttcomb"},"nBTags_MV2c10_70>=3&&nJets>=8")'
##
#root -l -b -q 'CreateNtuple.cpp("ttcomb_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttcomb"},"nBTags_MV2c10_70>=3&&nJets==7")'
##
#root -l -b -q 'CreateNtuple.cpp("ttcomb_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttcomb"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#
#
#root -l -b -q 'CreateNtuple.cpp("data_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"data"},"nBTags_MV2c10_70>=3&&nJets>=8")'
##
#root -l -b -q 'CreateNtuple.cpp("data_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"data"},"nBTags_MV2c10_70>=3&&nJets==7")'
##
#root -l -b -q 'CreateNtuple.cpp("data_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"data"},"nBTags_MV2c10_70>=3&&nJets==6")'
##
##
##
#root -l -b -q 'CreateNtuple.cpp("ttX_simon_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttX_simon"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#
#root -l -b -q 'CreateNtuple.cpp("ttX_simon_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttX_simon"},"nBTags_MV2c10_70>=3&&nJets==7")'
#
#root -l -b -q 'CreateNtuple.cpp("ttX_simon_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"ttX_simon"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#
#
#root -l -b -q 'CreateNtuple.cpp("vjets_simon_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"vjets_simon"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#
#root -l -b -q 'CreateNtuple.cpp("vjets_simon_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"vjets_simon"},"nBTags_MV2c10_70>=3&&nJets==7")'
#
#root -l -b -q 'CreateNtuple.cpp("vjets_simon_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"vjets_simon"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#
#
#root -l -b -q 'CreateNtuple.cpp("vv_simon_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"vv_simon"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#
#root -l -b -q 'CreateNtuple.cpp("vv_simon_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"vv_simon"},"nBTags_MV2c10_70>=3&&nJets==7")'
#
#root -l -b -q 'CreateNtuple.cpp("vv_simon_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"vv_simon"},"nBTags_MV2c10_70>=3&&nJets==6")'
#
#
#
#root -l -b -q 'CreateNtuple.cpp("single-top_simon_8ji3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"single-top_simon"},"nBTags_MV2c10_70>=3&&nJets>=8")'
#
#root -l -b -q 'CreateNtuple.cpp("single-top_simon_7je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"single-top_simon"},"nBTags_MV2c10_70>=3&&nJets==7")'
#
#root -l -b -q 'CreateNtuple.cpp("single-top_simon_6je3bi","/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/2LOS/mvatuple_5ji2bi/",{"single-top_simon"},"nBTags_MV2c10_70>=3&&nJets==6")'
