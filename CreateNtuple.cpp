#ifndef ROOTHEADERS
#define ROOTHEADERS
#include "TCanvas.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFile.h"
#include "TPad.h"
#include "TCutG.h"
#include "TCut.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TTreeFormula.h"
#include "TMath.h"
#include "TRint.h"
#include "TROOT.h"
#endif

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

using namespace std;




void CreateNtuple(
		  string nname = "ttbar_PhPy8_AFII_AllFilt_2b",
		  string file_location = "/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/",
		  vector<string> onname = {"ttbar_PhPy8_AFII_AllFilt"},
		  string s_cutg = "nBTags_MV2c10_70==2"
		  )
{


  TChain *chain = new TChain("nominal_Loose");

  cout << "FILES: " << endl;
  for (int i=0;i<onname.size();i++){
    chain->Add(Form("%s%s.root"
		    ,file_location.c_str(),onname[i].c_str()));
    cout << Form("%s%s.root",file_location.c_str(),onname[i].c_str()) << endl;
  }
  
  cout << "Chain has been created. Trees copying to the Ntuple..." << endl;
  cout << "======================================================" << endl;

  
  chain->LoadTree(0);
  TTreeFormula *selection = new TTreeFormula("selection",s_cutg.c_str(),chain);
  chain->SetNotify(selection);
  
  TFile *ntup_w = new TFile(Form("%s/%s.root",file_location.c_str(),nname.c_str()),"recreate");
  auto tree = chain->CloneTree(0);
  auto t0 = Clock::now();auto t1 = Clock::now();
  for (int i=0;i<(int)chain->GetEntries();i++){
    chain->LoadTree(i);
    chain->GetEntry(i);
    if (selection->EvalInstance())
      tree->Fill();

    if (i%((int)chain->GetEntries()/10)==0){
      t1 = Clock::now();
      cout << "Number of " << i << " events has been copied out of " << (int)chain->GetEntries()
	   << "  || Timing: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()/1000.0
	   << endl;
      t0 = Clock::now();
    }
  }
  
  cout << "======================================================" << endl;
  cout << "Tree has been copied. Saving into the ntuple..." << endl;

  tree->Write();
  ntup_w->Write();

  ntup_w->Close();

  
}
